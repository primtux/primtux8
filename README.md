# Primtux8

Basée sur [Linuxmint virginia 21.3 xfce] (https://www.linuxmint.com/edition.php?id=313)

## Compilation

Script de compilation de Primtux8 avec l'outil [Cubic] (https://github.com/PJ-Singh-001/Cubic)

### Créer un répertoire de travail

### Ouvrir Cubic

### Sélectionner l'iso de Linuxmint pour décompilation

### Lancement du terminal Cubic

### Import du dépôt Primtux8

~~~bash
root@cubic:~# echo "deb [signed-by=/usr/share/keyrings/PrimTux.gpg.key] https://mirrors.o2switch.fr/primtux/repo/debs PrimTux8 main" | tee /etc/apt/sources.list.d/primtux8.list
~~~

~~~bash
root@cubic:~# wget -O- https://mirrors.o2switch.fr/primtux/repo/debs/key/PrimTux.gpg.key | gpg --dearmor | tee /usr/share/keyrings/PrimTux.gpg.key > /dev/null
~~~

### Ajout de l'architecture i386

~~~bash
root@cubic:~# dpkg --add-architecture i386
~~~

### Mise à jour

~~~bash
root@cubic:~# apt update && sudo apt dist-upgrade -y
~~~

### Suppression des mint-artwork, polices et langues inutiles

~~~bash
root@cubic:~# apt autoremove mint-artwork mint-backgrounds-vanessa mint-backgrounds-vera mint-backgrounds-victoria mint-backgrounds-virginia mint-cursor-themes \
 mint-l-icons mint-l-theme mint-meta-core mint-meta-xfce mint-themes mint-x-icons mint-y-icons mintwelcome fonts-beng fonts-beng-extra fonts-deva fonts-deva-extra \
fonts-gargi fonts-gubbi fonts-gujr fonts-gujr-extra fonts-guru fonts-guru-extra fonts-indic fonts-kacst fonts-kacst-one fonts-kalapi fonts-khmeros-core fonts-knda \
fonts-lao fonts-lklug-sinhala fonts-lohit-beng-assamese fonts-lohit-beng-bengali fonts-lohit-deva fonts-lohit-gujr fonts-lohit-guru fonts-lohit-knda fonts-lohit-mlym \
fonts-lohit-orya fonts-lohit-taml fonts-lohit-taml-classical fonts-lohit-telu fonts-mlym fonts-nakula fonts-navilu fonts-noto-cjk fonts-orya fonts-orya-extra \
fonts-pagul fonts-sahadeva fonts-samyak-deva fonts-samyak-gujr fonts-samyak-mlym fonts-samyak-taml fonts-sarai fonts-sil-abyssinica fonts-sil-padauk fonts-smc \
fonts-smc-anjalioldlipi fonts-smc-chilanka fonts-smc-dyuthi fonts-smc-gayathri fonts-smc-karumbi fonts-smc-keraleeyam fonts-smc-manjari fonts-smc-meera \
fonts-smc-rachana fonts-smc-raghumalayalamsans fonts-smc-suruma fonts-smc-uroob fonts-taml fonts-telu fonts-telu-extra fonts-teluguvijayam fonts-thai-tlwg \
fonts-tibetan-machine fonts-tlwg-garuda fonts-tlwg-garuda-ttf fonts-tlwg-kinnari fonts-tlwg-kinnari-ttf fonts-tlwg-laksaman fonts-tlwg-laksaman-ttf fonts-tlwg-loma \
fonts-tlwg-loma-ttf fonts-tlwg-mono fonts-tlwg-mono-ttf fonts-tlwg-norasi fonts-tlwg-norasi-ttf fonts-tlwg-purisa fonts-tlwg-purisa-ttf fonts-tlwg-sawasdee \
fonts-tlwg-sawasdee-ttf fonts-tlwg-typewriter fonts-tlwg-typewriter-ttf fonts-tlwg-typist fonts-tlwg-typist-ttf fonts-tlwg-typo fonts-tlwg-typo-ttf fonts-tlwg-umpush \
fonts-tlwg-umpush-ttf fonts-tlwg-waree fonts-tlwg-waree-ttf hunspell-de-at-frami hunspell-de-ch-frami hunspell-de-de-frami hunspell-en-au hunspell-en-ca hunspell-en-gb \
hunspell-en-us hunspell-en-za hunspell-es hunspell-it hunspell-pt-br hunspell-pt-pt hunspell-ru hyphen-de hyphen-en-ca hyphen-en-gb hyphen-en-us hyphen-it hyphen-pt-br \
hyphen-pt-pt hyphen-ru language-pack-en language-pack-en-base language-pack-gnome-en language-pack-gnome-en-base mythes-de mythes-de-ch mythes-en-au mythes-en-us \
mythes-it mythes-pt-pt mythes-ru libreoffice-help-de libreoffice-help-en-gb libreoffice-help-en-us libreoffice-help-es libreoffice-help-it libreoffice-help-pt \
libreoffice-help-pt-br libreoffice-help-ru libreoffice-help-zh-cn libreoffice-help-zh-tw libreoffice-l10n-de libreoffice-l10n-en-gb libreoffice-l10n-en-za \
libreoffice-l10n-es libreoffice-l10n-it libreoffice-l10n-pt libreoffice-l10n-pt-br libreoffice-l10n-ru libreoffice-l10n-zh-cn libreoffice-l10n-zh-tw rhythmbox \
rhythmbox-data rhythmbox-plugin-tray-icon rhythmbox-plugins wngerman wogerman wportuguese wspanish wamerican wbrazilian wbritish witalian ubiquity-slideshow-mint \
blueman adwaita-icon-theme-full gnome-themes-extra gnome-themes-extra-data papirus-icon-theme yaru-theme-icon numix-icon-theme linux-headers-5.15.0-91-generic \
linux-headers-5.15.0-91 linux-image-5.15.0-91-generic linux-modules-5.15.0-91-generic linux-modules-extra-5.15.0-91-generic linux-headers-5.15.0-105-generic \
linux-headers-5.15.0-105 linux-image-5.15.0-102-generic linux-modules-5.15.0-105-generic linux-modules-extra-5.15.0-105-generic
~~~

### Installation des applications Primtux

~~~bash
root@cubic~# apt install --no-install-recommends abuledu-associations-primtux abuledu-microtexte abuledu-minitexte anki audacity dizaines dodoc-primtux dvdauthor \
dvgrab encoder-mots fet firefox firefox-locale-fr fotowall gcompris-qt geogebra5 goldendict gspeech hyphen-fr imagination jclic kdenlive khangman kiwix ktuberling \
kturtle language-pack-fr language-pack-fr-base language-pack-gnome-fr language-pack-gnome-fr-base leconjugueurlinux libreoffice libreoffice-help-fr libreoffice-l10n-fr \
lighdm-web-greeter-neon-theme-primtux web-greeter lumi marble minetest ocrfeeder openboard osmo pdfsam pendu-peda-gtk pinta-primtux accueil-primtux8 gtkdialog xournal \
speech-dispatcher speech-dispatcher-pico winff-gtk2 qdictionnaire scratch-desktop scratchjr pylote-primtux ubiquity-slideshow-primtux ri-li tuxpaint qstopmotion \
soundconverter vlc plymouth-theme-primtux-logo tcl tcl-snack tcl8.6 tk tk8.6 libtk-img samba usrmerge profil-poe-primtux profil-koda-primtux profil-jerry-primtux \
profil-leon-primtux nextcloud-primtux police-luciole-primtux trash-cli primtux8-wallpapers frei0r-plugins mediainfo libqt5sql5-sqlite python3-tk tix multiples vlc-l10n \
primtux-samba-config python3 python3-lxml make imagemagick zenity ghostscript gnupg2 bc xdg-utils perl findutils coreutils pinentry-gtk2 gnupg-agent font-manager \
xfce4-clipman xfce4-clipman-plugin sunclock fonts-opendyslexic colloid-theme-primtux8 colloid-icons-primtux8 xfce4-goodies vokoscreen-ng tuxblocs tuxmath hardinfo \
linux-generic-hwe-22.04 linux-headers-6.5.0-28-generic linux-headers-generic-hwe-22.04 linux-hwe-6.5-headers-6.5.0-28 linux-image-6.5.0-28-generic \
linux-image-generic-hwe-22.04 linux-modules-6.5.0-28-generic linux-modules-extra-6.5.0-28-generic
~~~

### Suppression d'un raccourci inutile

~~~bash
root@cubic:~# rm -f /usr/share/applications/web-greeter.desktop
~~~

### Mise à jour des icônes des curseurs

~~~bash
root@cubic:~# gtk-update-icon-cache /usr/share/icons/breeze/
root@cubic:~# gtk-update-icon-cache /usr/share/icons/breeze-dark/
~~~

## Copie de fichiers de configuration (par glisser-déposer sur la fenêtre du terminal de Cubic)

~~~bash
root@cubic:~# cd ..
~~~

Copier `etc.tar.gz` et `usr.tar.gz`

~~~bash
root@cubic:~# tar xvzf ./etc.tar.gz && rm -f etc.tar.gz

root@cubic:~# tar xvzf ./usr.tar.gz && rm -f usr.tar.gz
~~~

### Suppression de l'écran d'ouverture de session de Linuxmint

~~~bash
root@cubic:~# apt autoremove slick-greeter
~~~

### Création du lien vers le curseur de souris light

~~~bash
root@cubic:~# ln -s -f /etc/X11/cursors/Breeze_Snow.theme /etc/alternatives/x-cursor-theme
~~~

### Création des utilisateurs

~~~bash
root@cubic:~# useradd --password oymab1U7DSUmg -m -k /etc/skel-poe poe -c "Poe"

root@cubic:~# useradd --password oymab1U7DSUmg -m -k /etc/skel-jerry jerry -c "Jerry"

root@cubic:~# useradd --password oymab1U7DSUmg -m -k /etc/skel-koda koda -c "Koda"

root@cubic:~# useradd --password oymab1U7DSUmg -m -k /etc/skel-leon leon -c "Léon"

root@cubic:~# usermod --password oymab1U7DSUmg root
~~~

~~~bash
root@cubic:~# groupadd primtuxmenuuser

root@cubic:~# groupadd -r nopasswdlogin

root@cubic:~# gpasswd -a jerry nopasswdlogin

root@cubic:~# gpasswd -a koda nopasswdlogin

root@cubic:~# gpasswd -a leon nopasswdlogin
~~~

~~~bash
root@cubic:~# adduser poe audio

root@cubic:~# adduser poe cdrom

root@cubic:~# adduser poe video

root@cubic:~# adduser poe sudo 

root@cubic:~# adduser poe netdev

root@cubic:~# adduser poe plugdev

root@cubic:~# adduser poe users

root@cubic:~# adduser poe lp

root@cubic:~# adduser poe lpadmin

root@cubic:~# adduser poe scanner

root@cubic:~# adduser poe sambashare

root@cubic:~# adduser poe primtuxmenuuser
~~~

~~~bash
root@cubic:~# adduser jerry audio

root@cubic:~# adduser jerry cdrom

root@cubic:~# adduser jerry video

root@cubic:~# adduser jerry netdev

root@cubic:~# adduser jerry plugdev

root@cubic:~# adduser jerry users

root@cubic:~# adduser jerry lp

root@cubic:~# adduser jerry scanner

root@cubic:~# adduser jerry primtuxmenuuser
~~~

~~~bash
root@cubic:~# adduser koda audio

root@cubic:~# adduser koda cdrom

root@cubic:~# adduser koda video

root@cubic:~# adduser koda netdev

root@cubic:~# adduser koda plugdev

root@cubic:~# adduser koda users

root@cubic:~# adduser koda lp

root@cubic:~# adduser koda scanner

root@cubic:~# adduser koda primtuxmenuuser
~~~

~~~bash
root@cubic:~# adduser leon audio

root@cubic:~# adduser leon cdrom

root@cubic:~# adduser leon video

root@cubic:~# adduser leon netdev

root@cubic:~# adduser leon plugdev

root@cubic:~# adduser leon users

root@cubic:~# adduser leon lp

root@cubic:~# adduser leon scanner

root@cubic:~# adduser leon primtuxmenuuser
~~~

~~~bash
root@cubic:~# chsh -s /bin/bash poe && chsh -s /bin/false jerry && chsh -s /bin/false koda &&chsh -s /bin/false leon
~~~

### Suppression de raccourcis

~~~bash
root@cubic:~# rm -r /usr/share/applications/marble*.desktop
~~~

### Suppression des squelettes utilisateurs

~~~bash
root@cubic:~# rm -r /etc/skel-poe && rm -r /etc/skel-jerry && rm -r /etc/skel-koda && rm -r /etc/skel-leon
~~~

### Mise à jour et installation du primtuxmenu

~~~bash
root@cubic:~# apt update && apt install primtuxmenu && chgrp primtuxmenuuser /etc/primtuxmenu/primtuxmenu.conf
~~~

### Passage en français (fr_FR-UTF8) et clavier en Azerty

~~~bash
root@cubic:~# dpkg-reconfigure locales

root@cubic:~# dpkg-reconfigure keyboard-configuration
~~~

### Paramétrage des droits sur /etc

~~~bash
root@cubic:~# chmod -R 0755 /etc
root@cubic:~# dpkg-reconfigure keyboard-configuration
~~~

### Nettoyage

~~~bash
root@cubic:~# apt-get autoremove -y

root@cubic:~# apt-get clean

root@cubic:~# apt-mark showmanual | grep -E "linux-.*[0-9]" | grep -v "hwe" | xargs -r 

root@cubic:~# apt-mark auto

root@cubic:~# cd /var/cache/apt/archives && sudo rm *.deb

root@cubic:~# pkg --purge $(COLUMNS=200 dpkg -l | grep "^rc" | tr -s ' ' | cut -d ' ' -f 2)

root@cubic:~# rm -rf /tmp/* ~/.bash_history

root@cubic:~# update-initramfs -u
~~~

### Décompression de custom-disk.tar.gz dans custom-disk du répertoire de travail

Supprimer et remplacer les répertoires boot, EFI, isolinux et intégrer le répertoire preseed

### Choisir le noyau linux le plus récent

### Cocher la suppression d'ubiquity-slideshow-primtux en post-installation

### Choisir la compression gz du squashfs

### Patienter...

## Auteurs et remerciements

[Collectif-Primtux] (https://primtux.fr/blog/2015/11/25/primtux-lequipe/)

## Licence

CC-BY-SA